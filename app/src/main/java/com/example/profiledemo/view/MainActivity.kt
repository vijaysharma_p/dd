package com.example.profiledemo.view

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.profiledemo.App
import com.example.profiledemo.R
import com.example.profiledemo.databinding.ActivityMainBinding
import com.example.profiledemo.enums.Status
import com.example.profiledemo.model.Blog
import com.example.profiledemo.network.ApiResponse
import com.example.profiledemo.toolbarSimple
import com.example.profiledemo.utils.PreferenceUtil
import com.example.profiledemo.utils.ViewModelFactory
import com.example.profiledemo.view.Adapter.DataAdapter
import com.example.profiledemo.view_model.DataViewModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    var progressDialog: ProgressDialog? = null
    var activityMainBinding: ActivityMainBinding? = null
    var dataViewModel: DataViewModel? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage("loading please wait...")
        activityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)
        App.appComponent!!.doInjection(this)
        dataViewModel = ViewModelProviders.of(this, viewModelFactory).get(DataViewModel::class.java)
        dataViewModel?.getMutableLiveData()?.observe(this,
            Observer<ApiResponse?> { apiResponse ->
                apiResponse?.let { setdata(it) }
            })
        activityMainBinding?.actionLogout?.setOnClickListener {

                val myAlertDialog: AlertDialog.Builder = AlertDialog.Builder(this)
                myAlertDialog.setTitle("Logout")
                myAlertDialog.setMessage("are you sure logout?")
                myAlertDialog.setPositiveButton("No",
                    DialogInterface.OnClickListener { arg0, arg1 ->

                    })
                myAlertDialog.setNegativeButton("Yes",
                    DialogInterface.OnClickListener { arg0, arg1 ->
                        PreferenceUtil.putValue(PreferenceUtil.IS_LOGIN, false)
                        PreferenceUtil.save()
                        ContextCompat.startActivity(
                            this,
                            Intent(this, LoginActivity::class.java),
                            null
                        )
                        finish()
                    })
                myAlertDialog.show()

        }
    }

    private fun setdata(apiResponse: ApiResponse) {
        Log.d("asdas", apiResponse.status.toString());
        when (apiResponse.status) {
            Status.ERROR -> progressDialog?.dismiss()
            Status.LOADING -> progressDialog?.show()
            Status.SUCCESS -> {
                progressDialog?.dismiss()
                apiResponse.data?.let { setDataToView(it) }
            }
        }
    }

    private fun setDataToView(data: JsonElement) {
        val postResponse: Blog = Gson().fromJson(data.toString(), Blog::class.java)
        val dataAdapter = DataAdapter(postResponse, this)
        activityMainBinding?.rvData?.adapter = dataAdapter
    }
}