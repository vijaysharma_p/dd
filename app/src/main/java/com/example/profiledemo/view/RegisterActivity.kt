package com.example.profiledemo.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.example.profiledemo.R
import com.example.profiledemo.databinding.ActivityRegisterBinding
import com.example.profiledemo.view_model.RegisterViewModel

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityMainBinding: ActivityRegisterBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_register)
        activityMainBinding.viewmodel = RegisterViewModel(this)
        activityMainBinding.executePendingBindings()
    }
    companion object{
        @JvmStatic
        @BindingAdapter("toastMessage")
        fun runMe(view: View, message: String?) {
            if (message != null) {
                Toast.makeText(view.context, message + "", Toast.LENGTH_SHORT).show()
            }
        }
    }
}