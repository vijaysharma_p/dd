package com.example.profiledemo.view

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.profiledemo.App
import com.example.profiledemo.R
import com.example.profiledemo.databinding.ActivityProfileBinding
import com.example.profiledemo.enums.Status
import com.example.profiledemo.model.BlogItem
import com.example.profiledemo.network.ApiResponse
import com.example.profiledemo.toolbarSimple
import com.example.profiledemo.toolbarback
import com.example.profiledemo.utils.ViewModelFactory
import com.example.profiledemo.view_model.ProfileViewModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import java.io.File
import javax.inject.Inject


class ProfileActivity : AppCompatActivity() {

    var progressDialog: ProgressDialog? = null
    var activityProfileBinding: ActivityProfileBinding? = null
    var profileViewModel: ProfileViewModel? = null
    var blogid:Int=0
    val CAMERA_REQUEST = 0
    val GALLERY_PICTURE = 1

    val FILE_PROVIDER_AUTHORITY = "com.example.profiledemo.fileprovider"

    var selectedImagePath: String? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        blogid=intent.getIntExtra("id",1)
        Log.d("blogid",blogid.toString())
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage("loading please wait...")
        activityProfileBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_profile)
        App.appComponent!!.doInjection(this)
        profileViewModel = ViewModelProviders.of(this,viewModelFactory).get(ProfileViewModel::class.java)
        activityProfileBinding?.ivProfileImageSmall?.setOnClickListener {
            startDialog()
        }
        profileViewModel?.getMutableLiveData(blogid)?.observe(this,
            Observer<ApiResponse?> { apiResponse ->
                apiResponse?.let { setdata(it) }
            })
        toolbarback(activityProfileBinding?.toolBar!!,"Profile Detail")
    }

    private fun setdata(apiResponse: ApiResponse) {
        when (apiResponse.status) {
            Status.ERROR -> progressDialog?.dismiss()
            Status.LOADING -> progressDialog?.show()
            Status.SUCCESS -> {
                progressDialog?.dismiss()
                apiResponse.data?.let { setDataToView(it) }
            }
        }
    }

    private fun setDataToView(data: JsonElement) {
        val blogItem: BlogItem = Gson().fromJson(data.toString(), BlogItem::class.java)
        profileViewModel?.albumId = blogItem.albumId;
        profileViewModel?.id = blogItem.id;
        profileViewModel?.thumbnailUrl = blogItem.thumbnailUrl;
        profileViewModel?.title = blogItem.title;
        profileViewModel?.url = blogItem.url;
        activityProfileBinding?.tvData?.text = blogItem.title


    }


    private fun startDialog() {
        val myAlertDialog: AlertDialog.Builder = AlertDialog.Builder(this)
        myAlertDialog.setTitle("Upload Pictures Option")
        myAlertDialog.setMessage("How do you want to set your picture?")
        myAlertDialog.setPositiveButton("Gallery",
            DialogInterface.OnClickListener { arg0, arg1 ->
               var pictureActionIntent = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(
                    pictureActionIntent,
                    GALLERY_PICTURE
                )
            })
        myAlertDialog.setNegativeButton("Camera",
            DialogInterface.OnClickListener { arg0, arg1 ->
                val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(callCameraIntent, CAMERA_REQUEST)
            })
        myAlertDialog.show()
    }

    private fun buildFileProviderUri(uri: File): Uri? {
        return FileProvider.getUriForFile(
            this,
            FILE_PROVIDER_AUTHORITY,
            uri
        )
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var bitmap:Bitmap? = null
        selectedImagePath = null
        if (resultCode == Activity.RESULT_OK && requestCode == CAMERA_REQUEST) {
            try {
                bitmap = data?.extras!!.get("data") as Bitmap
                // preview image
                bitmap = Bitmap.createScaledBitmap(bitmap, 400, 400, false)
                activityProfileBinding?.ivProfileImageSmall?.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == GALLERY_PICTURE) {
            if (data != null) {
                val selectedImage = data.data
                val filePath =
                    arrayOf(MediaStore.Images.Media.DATA)
                val c = contentResolver.query(
                    selectedImage!!, filePath,
                    null, null, null
                )
                c!!.moveToFirst()
                val columnIndex = c.getColumnIndex(filePath[0])
                selectedImagePath = c.getString(columnIndex)
                c.close()
                if (selectedImagePath != null) {
                    //txt_image_path.setText(selectedImagePath)
                }
                bitmap = BitmapFactory.decodeFile(selectedImagePath) // load
                // preview image
                bitmap = Bitmap.createScaledBitmap(bitmap, 400, 400, false)
                activityProfileBinding?.ivProfileImageSmall?.setImageBitmap(bitmap)
            } else {
                Toast.makeText(
                    applicationContext, "Cancelled",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

}