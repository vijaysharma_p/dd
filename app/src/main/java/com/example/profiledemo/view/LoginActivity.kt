package com.example.profiledemo.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.example.profiledemo.R
import com.example.profiledemo.databinding.ActivityLoginBinding
import com.example.profiledemo.utils.PreferenceUtil
import com.example.profiledemo.view_model.LoginViewModel

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (PreferenceUtil.getBoolean(PreferenceUtil.IS_LOGIN, false)) {
            ContextCompat.startActivity(
                this,
                Intent(this, MainActivity::class.java),
                null
            )
            finish()
        }
        val activityMainBinding =
            DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        activityMainBinding.viewmodel = LoginViewModel(this)
        activityMainBinding.executePendingBindings()

    }

    companion object {
        @JvmStatic
        @BindingAdapter("toastMessage")
        fun runMe(view: View, message: String?) {
            if (message != null) {
                Toast.makeText(view.context, message + "", Toast.LENGTH_SHORT).show()
            }
        }
    }
}