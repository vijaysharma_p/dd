package com.example.profiledemo.view.Adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.profiledemo.R
import com.example.profiledemo.databinding.RawDataBinding
import com.example.profiledemo.model.BlogItem
import com.example.profiledemo.view.MainActivity
import com.example.profiledemo.view.ProfileActivity

class DataAdapter(
    var value: List<BlogItem?>?,
    var mainActivity: MainActivity
) : RecyclerView.Adapter<DataAdapter.MyViewholder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewholder {
        return MyViewholder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.raw_data, parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewholder, position: Int) {
        val dataModel: BlogItem = value?.get(position)!!
        holder.bind(dataModel)
        holder.itemView.setOnClickListener {
            val intent = Intent(mainActivity, ProfileActivity::class.java)
            intent.putExtra("id", value?.get(position)!!.id)
            mainActivity.startActivity(intent) }
    }

    override fun getItemCount(): Int {
        return value!!.size
    }

    class MyViewholder(var rawDataBinding: RawDataBinding) :
        RecyclerView.ViewHolder(rawDataBinding.root) {
        fun bind(dataModel: BlogItem?) {
            rawDataBinding.rawdata = dataModel
            rawDataBinding.executePendingBindings()
        }

    }
}