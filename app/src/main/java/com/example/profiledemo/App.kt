package com.example.profiledemo

import android.app.Application
import android.content.Context
import com.example.profiledemo.interfaces.AppComponent
import com.example.profiledemo.interfaces.DaggerAppComponent
import com.example.profiledemo.network.ApiModule
import com.example.profiledemo.utils.AppModule
import com.example.profiledemo.utils.PreferenceUtil


class App : Application() {
    companion object{
        var appComponent: AppComponent? = null
        var app: Context? = null
    }
    override fun onCreate() {
        super.onCreate()
        app = this
        PreferenceUtil.init(app!!)
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).apiModule(ApiModule()).build()
    }

    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
    }
}



