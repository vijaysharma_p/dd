package com.example.profiledemo.model

import android.R
import android.widget.ImageView

import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.util.*


data class BlogItem(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
) {
    companion object {
        @JvmStatic
        @BindingAdapter("loadSmallImage")
        fun loadSmallImage(imageView: ImageView, imageURL: String?) {
            Glide.with(imageView.context)
                .asBitmap()
                .apply(RequestOptions.circleCropTransform())
                .load("https://files.ontario.ca/small-biz-advice.png")
                .into(imageView)
        }

        @JvmStatic
        @BindingAdapter("loadBigImage")
        fun loadBigImage(imageView: ImageView, imageURL: String?) {
            Glide.with(imageView.context)
                .asBitmap()
                .apply(RequestOptions.centerCropTransform())
                .load("https://images.all-free-download.com/images/graphiclarge/small_mushrooms_200614.jpg")
                .into(imageView)
        }
    }
}

// important code for loading image here
