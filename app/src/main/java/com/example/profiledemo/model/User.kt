package com.example.profiledemo.model

data class User(
var email: String,
var password: String,
var userName: String
)