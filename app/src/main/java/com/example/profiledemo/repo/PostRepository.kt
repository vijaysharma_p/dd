package com.example.profiledemo.repo

import com.example.profiledemo.interfaces.UsersService
import com.google.gson.JsonElement
import io.reactivex.Observable
import javax.inject.Inject

class PostRepository @Inject constructor(var usersService: UsersService) {
    val posts: Observable<JsonElement?>?
        get() = usersService.fetchPosts("photos/")

    fun getSinglePosts(id:Int): Observable<JsonElement?>? {
        return usersService.fetchPosts("posts/${id}")
    }
}
