package com.example.profiledemo.network

import androidx.lifecycle.ViewModelProvider
import com.example.profiledemo.interfaces.AppConstant
import com.example.profiledemo.interfaces.AppConstant.Companion.BASE_URL
import com.example.profiledemo.interfaces.UsersService
import com.example.profiledemo.repo.PostRepository
import com.example.profiledemo.utils.ViewModelFactory
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class ApiModule : AppConstant {
    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder =
            GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.setLenient().create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson?, okHttpClient: OkHttpClient?): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    @Singleton
    fun getApiCallInterface(retrofit: Retrofit): UsersService {
        return retrofit.create(UsersService::class.java)
    }

    @get:Singleton
    @get:Provides
    val requestHeader: OkHttpClient
        get() {
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor { chain: Interceptor.Chain ->
                val original = chain.request()
                val request = original.newBuilder().build()
                chain.proceed(request)
            }.connectTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(100, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS)
            return httpClient.build()
        }

    fun getreposiotory(apiCallInterface: UsersService?): PostRepository {
        return PostRepository(apiCallInterface!!)
    }

    fun getViewModelFactory(postRepository: PostRepository?): ViewModelProvider.Factory {
        return ViewModelFactory(postRepository!!)
    }
}
