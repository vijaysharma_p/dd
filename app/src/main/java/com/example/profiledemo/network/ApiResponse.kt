package com.example.profiledemo.network

import android.util.Log
import com.example.profiledemo.enums.Status
import com.google.gson.JsonElement


class ApiResponse private constructor(
    var status: Status,
    var data: JsonElement?,
    var error: Throwable?
) {

    companion object {
        fun loading(): ApiResponse {
            return ApiResponse(Status.LOADING, null, null)
        }

        fun success(data: JsonElement): ApiResponse {
            Log.d("aaaaadata",data.toString())
            return ApiResponse(Status.SUCCESS, data, null)
        }

        fun error(error: Throwable): ApiResponse {
            Log.d("aaaaadata",error.localizedMessage)
            return ApiResponse(Status.ERROR, null, error)
        }
    }

}