package com.example.profiledemo.enums

enum class Status {
    LOADING, SUCCESS, ERROR, COMPLETED
}