package com.example.profiledemo.interfaces

interface AppConstant {
    companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    }
}
