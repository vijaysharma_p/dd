package com.example.profiledemo.interfaces

import com.example.profiledemo.network.ApiModule
import com.example.profiledemo.utils.AppModule
import com.example.profiledemo.view.MainActivity
import com.example.profiledemo.view.ProfileActivity
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, ApiModule::class])
@Singleton
interface AppComponent {
    fun doInjection(mainActivity: MainActivity?)
    fun doInjection(profileActivity: ProfileActivity?)
}