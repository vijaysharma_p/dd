package com.example.profiledemo.interfaces

import com.google.gson.JsonElement
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface UsersService {
    @GET
    fun fetchPosts(@Url url: String?): Observable<JsonElement?>?
}
