package com.example.profiledemo.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.profiledemo.model.Blog
import com.example.profiledemo.repo.PostRepository
import com.example.profiledemo.view_model.DataViewModel
import com.example.profiledemo.view_model.ProfileViewModel
import javax.inject.Inject


class ViewModelFactory @Inject constructor(var postRepository: PostRepository) :
    ViewModelProvider.Factory {
    var postRepositorys: PostRepository = postRepository

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DataViewModel::class.java)) {
            return DataViewModel(postRepositorys) as T
        }else if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
            return ProfileViewModel(postRepositorys) as T
        }
        throw java.lang.IllegalArgumentException("unknown class")
    }
}
