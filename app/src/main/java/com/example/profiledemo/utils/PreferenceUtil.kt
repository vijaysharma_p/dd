package com.example.profiledemo.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson

/**
 * Creates SharedPreference for the application. and provides access to it
 *
 */

object PreferenceUtil {

    private var sharedPreferences: SharedPreferences? = null

    private var editor: SharedPreferences.Editor? = null

    /**
     * returns map of all the key value pair available in SharedPreference
     *
     * @return Map<String></String>, ?>
     */
    val all: Map<String, *>
        get() = sharedPreferences!!.all

    /**
     * Initialize bathe SharedPreferences instance for the app.
     * This method must be called before using any other methods of this class.
     */

    fun init(mcontext: Context) {
        if (sharedPreferences == null) {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mcontext)
            editor = sharedPreferences!!.edit()
        }
    }


    fun removeUserData() {
        putValue(PreferenceUtil.IS_LOGIN, false)
        remove(PreferenceUtil.USER_EMAIL)
        remove(PreferenceUtil.USER_NAME)
        remove(PreferenceUtil.PASSWORD)
        save()
    }

    /**
     * Puts new Key and its Values into SharedPreference map.
     *
     * @param key
     * @param value
     */
    fun putValue(key: String, value: String) {
        editor!!.putString(key, value)
    }

    /**
     * Puts new Key and its Values into SharedPreference map.
     *
     * @param key
     * @param value
     */
    fun putValue(key: String, sets : Set<String>) {
        editor!!.putStringSet(key, sets)
    }

    /**
     * Puts new Key and its Values into SharedPreference map.
     *
     * @param key
     * @param value
     */
    fun putValue(key: String, value: Int) {
        editor!!.putInt(key, value)
    }

    /**
     * Puts new Key and its Values into SharedPreference map.
     *
     * @param key
     * @param value
     */
    fun putValue(key: String, value: Long) {
        editor!!.putLong(key, value)
    }

    /**
     * Puts new Key and its Values into SharedPreference map.
     *
     * @param key
     * @param value
     */
    fun putValue(key: String, value: Boolean) {
        editor!!.putBoolean(key, value)
    }

    /**
     * saves the values from the editor to the SharedPreference
     */
    fun clearAll() {
        editor!!.clear()
    }

    fun remove(key: String) {
        editor!!.remove(key)
    }


    fun save() {
        editor!!.commit()
    }


    /**
     * returns a values card with a Key default value ""
     *
     * @return String
     */
    fun getStringSet(key: String,sets : Set<String>): Set<String>? {
        return sharedPreferences!!.getStringSet(key, sets)
    }

    /**
     * returns a values card with a Key default value ""
     *
     * @return String
     */
    fun getString(key: String, defValue: String): String? {
        return sharedPreferences!!.getString(key, defValue)
    }

    /**
     * returns a values card with a Key default value -1
     *
     * @return String
     */
    fun getInt(key: String, defValue: Int): Int {
        return sharedPreferences!!.getInt(key, defValue)
    }

    /**
     * returns a values card with a Key default value -1
     *
     * @return String
     */
    fun getLong(key: String, defValue: Long): Long {
        return sharedPreferences!!.getLong(key, defValue)
    }

    /**
     * returns a values card with a Key default value false
     *
     * @return String
     */
    fun getBoolean(key: String, defValue: Boolean): Boolean {
        return sharedPreferences!!.getBoolean(key, defValue)
    }

    /**
     * Checks if key is exist in SharedPreference
     *
     * @param key
     * @return boolean
     */
    operator fun contains(key: String): Boolean {
        return sharedPreferences!!.contains(key)
    }

    /**
     * Puts new Key and its Values (class object) into SharedPreference map.
     *
     * @param key
     * @param value
     */
    fun putData(key: String, any: Any) {
        try {
            val gson = Gson()
            val jsonData = gson.toJson(any)
            editor?.putString(key, jsonData)
            editor?.apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * returns a values card with a Key default value null
     *
     * @return class object
     */
    fun getData(key: String, classname: Class<*>): Any? {
        val gson = Gson()
        val json = sharedPreferences?.getString(key, null)
        return if (json == null)
            null
        else
            gson.fromJson<Any>(json, classname)
    }



    const val IS_LOGIN = "IsFirstTimeLaunch"
    const val USER_EMAIL = "user_email"
    const val USER_NAME: String = "user_name"
    const val PASSWORD: String = "password"

}