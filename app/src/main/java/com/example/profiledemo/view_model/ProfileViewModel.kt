package com.example.profiledemo.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.profiledemo.network.ApiResponse
import com.example.profiledemo.repo.PostRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class ProfileViewModel : ViewModel {
    private val compositeDisposable = CompositeDisposable()
    // private lateinit var liveData: MutableLiveData<ApiResponse>
    private val liveData = MutableLiveData<ApiResponse>()
    private lateinit var postRepository: PostRepository

    var albumId: Int?=null;
    var id: Int?=null;
    var thumbnailUrl: String?=null;
    var title: String?=null;
    var url: String?=null;

    constructor(){
    }
    constructor(postRepository: PostRepository){
        this.postRepository = postRepository;
    }

    fun getMutableLiveData(blogid: Int): MutableLiveData<ApiResponse>? {
        getPosts(blogid)
        return liveData
    }

    fun getPosts(blogid: Int) {
        postRepository.getSinglePosts(blogid)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnSubscribe { d -> liveData.setValue(ApiResponse.loading()) }
            ?.subscribe(
                { result -> liveData.setValue(result?.let { ApiResponse.success(it) }) },
                { throwable -> liveData.setValue(ApiResponse.error(throwable)) }
            )?.let {
                compositeDisposable.add(
                    it
                )
            }
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }

}