package com.example.profiledemo.view_model

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.profiledemo.BR
import com.example.profiledemo.model.User
import com.example.profiledemo.utils.PreferenceUtil
import com.example.profiledemo.view.LoginActivity
import com.example.profiledemo.view.MainActivity
import com.example.profiledemo.view.ProfileActivity
import com.example.profiledemo.view.RegisterActivity

class LoginViewModel(
    var loginActivity: LoginActivity
) : BaseObservable() {
    private var user = User("", "", "")

    @Bindable
    private var toastMessage: String? = null

    @Bindable
    fun getUserEmail(): String? {
        return user.email
    }

    @Bindable
    fun getUserPassword(): String? {
        return user.password
    }

    fun setUserEmail(email: String) {
        user.email = email
        notifyPropertyChanged(BR.userEmail)
    }

    fun setUserPassword(password: String) {
        user.password = password
        notifyPropertyChanged(BR.userPassword)
    }

    fun getToastMessage(): String? {
        return toastMessage
    }

    private fun setToastMessage(toastMessage: String) {
        this.toastMessage = toastMessage
        notifyPropertyChanged(BR.toastMessage)
    }

    fun OnLoginonClick() {
        if (isInputValid()) {
            var successMessage: String? = null
            if (checkLogin()) {
                PreferenceUtil.putValue(PreferenceUtil.IS_LOGIN, true)
                PreferenceUtil.save()
                successMessage = "Login was successful"
                startActivity(loginActivity, Intent(loginActivity, MainActivity::class.java), null)
                loginActivity.finish()
            } else {
                successMessage = "Email or Password is wrong"
            }
            setToastMessage(successMessage)
        } else {

            val errorMessage = "Email or Password not valid"
            setToastMessage(errorMessage)
        }
    }

    private fun checkLogin(): Boolean {
        return PreferenceUtil.getString(
                PreferenceUtil.USER_EMAIL,
                ""
            ) == user.email && PreferenceUtil.getString(
                PreferenceUtil.PASSWORD,
                ""
            ) == user.password
    }

    fun OnSignUp() {
        startActivity(loginActivity, Intent(loginActivity, RegisterActivity::class.java), null)
        loginActivity.finish()
    }

    private fun isInputValid(): Boolean {
        return !TextUtils.isEmpty(user.email) && !TextUtils.isEmpty(user.password)
    }
}