package com.example.profiledemo.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.profiledemo.network.ApiResponse
import com.example.profiledemo.repo.PostRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class DataViewModel: ViewModel {
    private val compositeDisposable = CompositeDisposable()
    // private lateinit var liveData: MutableLiveData<ApiResponse>
    private val liveData = MutableLiveData<ApiResponse>()
    private lateinit var postRepository: PostRepository

    constructor(){
    }
    constructor(postRepository: PostRepository){
        this.postRepository = postRepository;
    }

    fun getMutableLiveData(): MutableLiveData<ApiResponse>? {
        getPosts()
        return liveData
    }

    fun getPosts() {
        postRepository.posts
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnSubscribe { d -> liveData.setValue(ApiResponse.loading()) }
            ?.subscribe(
                { result -> liveData.setValue(result?.let { ApiResponse.success(it) }) },
                { throwable -> liveData.setValue(ApiResponse.error(throwable)) }
            )?.let {
                compositeDisposable.add(
                    it
                )
            }
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }

}