package com.example.profiledemo.view_model

import android.content.Intent
import android.text.TextUtils
import android.util.Log
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.profiledemo.BR
import com.example.profiledemo.model.User
import com.example.profiledemo.utils.PreferenceUtil
import com.example.profiledemo.view.LoginActivity
import com.example.profiledemo.view.RegisterActivity
import java.util.regex.Pattern

class RegisterViewModel(var registerActivity:RegisterActivity) :BaseObservable(){
    val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )
    private var user = User("", "", "")

    @Bindable
    private var toastMessage: String? = null

    @Bindable
    fun getUserName(): String? {
        return user?.userName
    }

    @Bindable
    fun getUserEmail(): String? {
        return user?.email
    }

    @Bindable
    fun getUserPassword(): String? {
        return user?.password
    }

    fun setUserName(userName: String) {
        user?.userName = userName
        notifyPropertyChanged(BR.userName)
    }

    fun setUserEmail(email: String) {
        user?.email = email
        notifyPropertyChanged(BR.userEmail)
    }

    fun setUserPassword(password: String) {
        user?.password = password
        notifyPropertyChanged(BR.userPassword)
    }

    fun getToastMessage(): String? {
        return toastMessage
    }

    private fun setToastMessage(toastMessage: String) {
        this.toastMessage = toastMessage
        notifyPropertyChanged(BR.toastMessage)
    }

    fun OnRegisterClick() {
        if (isInputValid()) {
            PreferenceUtil.putValue(PreferenceUtil.USER_EMAIL, user.email)
            PreferenceUtil.putValue(PreferenceUtil.USER_NAME, user.password)
            PreferenceUtil.putValue(PreferenceUtil.PASSWORD, user.userName)
            PreferenceUtil.save()
            Log.d("USER_EMAIL", PreferenceUtil.getString(PreferenceUtil.USER_EMAIL,"").toString())
            Log.d("USER_NAME",PreferenceUtil.getString(PreferenceUtil.USER_EMAIL,"").toString())
            Log.d("PASSWORD",PreferenceUtil.getString(PreferenceUtil.PASSWORD, "").toString())
            val successMessage = "Register was successful"
            setToastMessage(successMessage)
        } else {
            val errorMessage = "Email or Password not valid"
            setToastMessage(errorMessage)
        }
    }

    fun OnLoginClick() {
       startActivity(registerActivity, Intent(registerActivity, LoginActivity::class.java), null)
        registerActivity?.finish()
    }

    private fun isInputValid(): Boolean {
        return !TextUtils.isEmpty(user.email) &&checkEmail(user.email) && !TextUtils.isEmpty(user.password) && !TextUtils.isEmpty(user.userName)
    }
    private fun checkEmail(email: String): Boolean {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches()
    }
}